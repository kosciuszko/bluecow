from distutils.core import setup

setup(name='bluecow',
      version='0.9.5',
      packages=['bluecow'],
      description='parent class for cloudformation custom resources',
      author='Kosciuszko Cloud',
      author_email='jonasmccallum@gmail.com',
      url='https://gitlab.com/kosciuszko/bluecow',
      license='http://www.opensource.org/licenses/mit-license.php',
      classifiers=['Intended Audience :: Developers',
                   'License :: OSI Approved :: MIT License',
                   'Natural Language :: English',
                   'Programming Language :: Python :: 3'],
      python_requires='>=3.6',
      install_requires=['requests'],
      long_description_content_type='text/markdown',
      keywords='cloudformation custom resource')
