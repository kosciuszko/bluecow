import requests

class CustomResource:

    def __init__(self, event, context):
        """harvest the event details"""
        self.event = event
        self.context = context
        self.action = event['RequestType']
        self.response_url =  event['ResponseURL']

    def execute(self):
        """lambda handler"""
        try:
            if self.action == 'Create':
                result = self.create()
            elif self.action == 'Update':
                result = self.update()
            elif self.action == 'Delete':
                result = self.delete()
            self.cfn_respond("SUCCESS", result)
        except exception as e:
            self.cfn_respond("FAILED", str(e))

    def create(self):
        """create the resource"""
        raise NotImplementedError

    def update(self):
        """update the resource"""
        raise NotImplementedError

    def delete(self):
        """delete the resource"""
        raise NotImplementedError

    def cfn_respond(self, status, data):
        """respond to cfn with the outcome"""
        body = {
            'Status': status,
            'Data': {'Data': data},
            'Reason': f'More details: {self.context.log_stream_name}',
            'PhysicalResourceId': self.context.log_stream_name,
            'StackId': self.event['StackId'],
            'RequestId': self.event['RequestId'],
            'LogicalResourceId': self.event['LogicalResourceId'],
            'NoEcho': False
        }
        return requests.put(self.response_url, json=body)
